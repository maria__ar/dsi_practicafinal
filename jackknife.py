#!/usr/bin/env python
# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import numpy


from scipy import cluster, stats
from sklearn import preprocessing
import sklearn.neighbors

from sklearn.decomposition import PCA

import loaddata
def jackknife(path):
    data = loaddata.load_data(path)
    avSim_i = []
    outliers = []
    data_clean = []
    data_sin_out = []

    min_max_scaler = preprocessing.MinMaxScaler()
    data_norm = min_max_scaler.fit_transform(data)
    data_norm = data_norm.tolist()


    for i in range(len(data_norm)):
        data_norm_i = data_norm[:i-1]+data_norm[i+1:]

        dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
        matsim = dist.pairwise(data_norm_i)
        avSim = numpy.average(matsim)
        avSim_i.append(avSim)

    matsim = dist.pairwise(data_norm)
    avSim = numpy.average(matsim)

    umbral = 1.5
    sigma = numpy.std(avSim_i)
    print "Sigma: ", sigma

    for i in range(len(data_norm)):
        if (abs(avSim_i[i]-avSim)>umbral*sigma):
            outliers.append(i)
    print "Number of outliers: ", len(outliers)

    estimator = PCA (n_components = 2)
    X_pca = estimator.fit_transform(data_norm)

    fig, ax = plt.subplots()
    plt.xlim(-0.5, 1.5)
    plt.ylim(-0.4, 1)

    for i in range(len(X_pca)):
        if i in outliers:
            plt.text(X_pca[i][0], X_pca[i][1], 'x', color='k')
        else:
            plt.text(X_pca[i][0], X_pca[i][1], 'x', color='r')

    ax.grid(True)
    fig.tight_layout()
    plt.show()

    for i in range(len(data_norm)):
        if i not in outliers:
            data_clean.append(data_norm[i])
            data_sin_out.append(data[i])

    return data_clean, data_sin_out
