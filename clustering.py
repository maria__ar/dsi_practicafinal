#!/usr/bin/env python
# -*- coding: utf-8 -*-

import jackknife
import bic
import matplotlib.pyplot as plt
import numpy
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn import metrics

from scipy import cluster
import sklearn.neighbors

path = "head.csv"
n_clusters=7
data_clean, data = jackknife.jackknife(path)

dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
matsim = dist.pairwise(data_clean)
avSim = numpy.average(matsim)
kmeans = KMeans(n_clusters=n_clusters)
clusters = kmeans.fit(data)

plt.show()
estimator = PCA (n_components = 2)
X_pca = estimator.fit_transform(data_clean)
colors = numpy.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
colors = numpy.hstack([colors] * 20)
fig, ax = plt.subplots()
plt.xlim(-0.3, 0.6)
plt.ylim(-0.2, 0.6)

labels=kmeans.labels_

for i in range(len(X_pca)):
    plt.text(X_pca[i][0], X_pca[i][1], 'x', color=colors[labels[i]])

ax.grid(True)
fig.tight_layout()
plt.show()

groups = []
for c in range(0, n_clusters):
    print 'Grupo', c
    aux = []
    for i in range(len(data_clean[0])):
        column = [row[i] for j,row in enumerate(data) if labels[j] == c]
        if len(column) != 0:
            print i, numpy.mean(column)
            aux.append(int(round((numpy.mean(column)))))
    groups.append(aux)
    aux = []

x = numpy.arange(0, 1, 1)
ax = plt.subplot(111)
ax.bar(x-0.7, groups[0][2],width=0.15,color='b',align='center')
ax.bar(x-0.5, groups[1][2],width=0.15,color='g',align='center')
ax.bar(x-0.3, groups[2][2],width=0.15,color='r',align='center')
ax.bar(x-0.1, groups[3][2],width=0.15,color='c',align='center')
ax.bar(x+0.1, groups[4][2],width=0.15,color='m',align='center')
ax.bar(x+0.3, groups[5][2],width=0.15,color='y',align='center')
ax.bar(x+0.5, groups[6][2],width=0.15,color='k',align='center')
ax.legend(('Group 1', 'Group 2', 'Group 3', 'Group 4', 'Group 5', 'Group 6', 'Group 7'))
plt.xlabel("Propiedades")
plt.show()


