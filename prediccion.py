# -*- coding: utf-8 -*-

from sklearn.datasets import load_iris
from sklearn import tree
from IPython.display import Image
import sklearn.neighbors
from sklearn import preprocessing
import matplotlib.pyplot as plt
from matplotlib.pylab import hist, show
import numpy
import scipy
import scipy.spatial
from scipy import cluster
from sklearn.decomposition import PCA
import pydotplus
import loaddata

path = "cluster.csv"
data = loaddata.load_data(path)

data_clean = []
raid = []
print data[0]
for i in range(len(data)):
	aux =[]
	for j in range(len(data[0])):
		if(j==len(data[0])-1):
			raid.append(data[i][j])
		else:
			aux.append(data[i][j])
	data_clean.append(aux)
print data_clean

features = ["USUARIOS","FECHA","TARIFA","ACTIVA_TOTAL"]

classes = ["Grupo 1", "Grupo 2", "Grupo 3", "Grupo 4", "Grupo 5", "Grupo 6", "Grupo 7"]

clf = tree.DecisionTreeClassifier()
clf = clf.fit(data_clean, raid)

from sklearn.externals.six import StringIO
import pydot

## Extract the decision tree logic from the trained model
dot_data = StringIO()
tree.export_graphviz(clf, out_file=dot_data,
                         feature_names=features,
                         #class_names=clf.classes_,
                         class_names=classes,
                         filled=True, rounded=True,
                         special_characters=True)

## convert the logics into graph
graph = pydot.graph_from_dot_data(dot_data.getvalue())

## This will plot decision tree in pdf file
graph[0].write_pdf(path="endesa.pdf")
